import tensorflow as tf
import hls4ml
import numpy as np
from deepcalo.layers import FiLM, Slice_tensor1D,Sum1D, Mask_track
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import mean_squared_error
from scipy.stats import iqr
import pydot


custom_objects = {
                'Slice_tensor1D'    :   Slice_tensor1D,
                'FiLM'              :   FiLM,
                'Sum1D'             :   Sum1D,
                'Mask_track'        :   Mask_track
                }

#Load Model
model = tf.keras.models.load_model(filepath = './../final_model.h5',  custom_objects = custom_objects )
model.summary()



# Plot model
#dot_img_file = 'model.png'
#tf.keras.utils.vis_utils.plot_model(model, to_file=dot_img_file, show_shapes=True)


# inputs
DATA_NUM = 10000

# input image
x_test_em_barrel = np.load('./../x_test_em_barrel.npy').astype(np.float32)[0:DATA_NUM]

# input scalar
x_test_scalars = np.load('./../x_test_scalars.npy').astype(np.float32)[0:DATA_NUM]

# input track
x_test_tracks = np.load('./../x_test_tracks.npy').astype(np.float32)[0:DATA_NUM]

#output track 
y_test_tracks = np.load('./../y_test_targets.npy').astype(np.float32)[0:DATA_NUM]

#generate hls model
hls_path = 'Deepcalo_model'




config = hls4ml.utils.config_from_keras_model(model, granularity='model')
config['Model']['Strategy'] = 'Resource'
config['Model']['ReuseFactor'] = 2000

fix_number = 'ap_fixed<32,16>'
config['Model']['Precision'] = fix_number

hls_model = hls4ml.converters.convert_from_keras_model(model,
                                                           io_type='io_stream',
                                                           hls_config=config,
                                                           backend='VivadoAccelerator',
                                                           output_dir=hls_path,
                                                           part="xczu9eg-ffvb1156-2-e")
                                                           
hls_model.compile()


# plot hls model
#hls4ml.utils.plot.plot_model(hls_model, to_file='Deepcalo.png', show_shapes=True, show_layer_names=True, show_precision=True, rankdir='TB', dpi=96)


