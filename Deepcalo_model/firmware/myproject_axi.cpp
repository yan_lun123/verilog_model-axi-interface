#include "myproject_axi.h"

  model_default_t w13[9216];
  model_default_t w18[18432];
  model_default_t w22[36864];
  model_default_t w27[73728];
  model_default_t w31[147456];
  model_default_t w36[294912];
  model_default_t w40[589824];
  model_default_t w45[589824];
  model_default_t w49[65536];


void myproject_axi(
    input_axi_t in[N_IN],
    output_axi_t out[N_OUT]
        ){

    #pragma HLS INTERFACE axis port=in
    #pragma HLS INTERFACE axis port=out
    #pragma HLS INTERFACE ap_ctrl_none port=return
    #pragma HLS DATAFLOW

    unsigned short in_size = 0;
    unsigned short out_size = 0;

    bool is_last = false;
    hls::stream<input_t> in_local("input_1");
    hls::stream<result_t> out_local("output_1");

    #pragma HLS STREAM variable=in_local depth=N_IN
    #pragma HLS STREAM variable=out_local depth=N_OUT

    for(unsigned i = 0; i < N_IN / 4; ++i) {
        input_t ctype;
        #pragma HLS DATA_PACK variable=ctype
        for(unsigned j = 0; j < 4; j++) {
            ctype = input_t(in[i * 4 + j].data);
            is_last |= (in[i * 4 + j].last == 1)? true : false;
            in_local.write(ctype);
        }
    }

    myproject(in_local, out_local, in_size, out_size,w13,w18,w22,w27,w31,w36,w40,w45,w49);

    for(unsigned i = 0; i < N_OUT / 1; ++i) {
        result_t ctype = out_local.read();
        for(unsigned j = 0; j < 1; j++) {
            bool last = (is_last && (i * 1 + j == N_OUT - 1)) ? true : false;
            out[i * 1 + j] = output_axi_t(ctype, last);
        }
    }
}
