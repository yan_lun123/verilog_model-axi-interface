// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V_H__
#define __dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V_H__


#include <systemc>
using namespace sc_core;
using namespace sc_dt;




#include <iostream>
#include <fstream>

struct dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V_ram : public sc_core::sc_module {

  static const unsigned DataWidth = 20;
  static const unsigned AddressRange = 256;
  static const unsigned AddressWidth = 8;

//latency = 1
//input_reg = 1
//output_reg = 0
sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in <sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


sc_lv<DataWidth> ram[AddressRange];


   SC_CTOR(dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V_ram) {
        ram[0] = "0b00000000000000000000";
        ram[1] = "0b11111111111111111111";
        for (unsigned i = 2; i < 7 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[7] = "0b11111111111111111111";
        for (unsigned i = 8; i < 19 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[19] = "0b00000000000111110010";
        ram[20] = "0b00000000000000000000";
        ram[21] = "0b00000000000000000000";
        ram[22] = "0b00000000000100101010";
        ram[23] = "0b00000000000000000000";
        ram[24] = "0b00000000000000000000";
        ram[25] = "0b00000000000000000000";
        ram[26] = "0b00000000000000000011";
        ram[27] = "0b00000000000000000000";
        ram[28] = "0b11111111111111111011";
        ram[29] = "0b00000000000000000010";
        ram[30] = "0b00000000000000000000";
        ram[31] = "0b00000000001010101110";
        for (unsigned i = 32; i < 37 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[37] = "0b00000000000010001010";
        ram[38] = "0b11111111111111111111";
        ram[39] = "0b00000000000011011101";
        ram[40] = "0b00000000000000000000";
        ram[41] = "0b00000000000000000000";
        ram[42] = "0b00000000000000000000";
        ram[43] = "0b00000000000000010001";
        ram[44] = "0b00000000000000000000";
        ram[45] = "0b00000000000000000000";
        ram[46] = "0b00000000000000000000";
        ram[47] = "0b11111111111111111111";
        ram[48] = "0b00000000000000000000";
        ram[49] = "0b00000000111001000100";
        ram[50] = "0b11111111111111111111";
        ram[51] = "0b00000000000000000000";
        ram[52] = "0b00000000000001111111";
        ram[53] = "0b00000000000000000000";
        ram[54] = "0b00000000000000000000";
        ram[55] = "0b11111111111110000111";
        ram[56] = "0b00000000000000101110";
        ram[57] = "0b00000000000000000000";
        ram[58] = "0b11111110011110011000";
        ram[59] = "0b00000000000000000000";
        ram[60] = "0b00000000000000000000";
        ram[61] = "0b11111111111100001100";
        ram[62] = "0b01001100111011101111";
        ram[63] = "0b00000000000000000000";
        ram[64] = "0b00000000000000000000";
        ram[65] = "0b11111111101000111010";
        ram[66] = "0b00000000000000000000";
        ram[67] = "0b00000000000000000010";
        ram[68] = "0b00000000000000000000";
        ram[69] = "0b00000000000000000000";
        ram[70] = "0b00000000000000000000";
        ram[71] = "0b00000000000000000001";
        ram[72] = "0b00100100010100110010";
        ram[73] = "0b00000000000000000000";
        ram[74] = "0b00000000000000000000";
        ram[75] = "0b00000000000000000000";
        ram[76] = "0b00000000000000011101";
        ram[77] = "0b00000000000000000000";
        ram[78] = "0b11111111111111111111";
        ram[79] = "0b00000000000000000000";
        ram[80] = "0b11111111111111000010";
        ram[81] = "0b11111111111111111111";
        ram[82] = "0b00000000000000000000";
        ram[83] = "0b00000000000000000000";
        ram[84] = "0b00000000000000000000";
        ram[85] = "0b00000000000000111000";
        ram[86] = "0b00000000000000000000";
        ram[87] = "0b00000000000000000000";
        ram[88] = "0b00000000000000000000";
        ram[89] = "0b00000000000000000000";
        ram[90] = "0b00000000000001101101";
        ram[91] = "0b00000000000000000000";
        ram[92] = "0b11111111111111111111";
        ram[93] = "0b00000000000000110000";
        ram[94] = "0b00000000000000000000";
        ram[95] = "0b00000000000000000000";
        ram[96] = "0b00000000000000000000";
        ram[97] = "0b00000001001000101111";
        ram[98] = "0b00000000000000000000";
        ram[99] = "0b00000000000001010010";
        ram[100] = "0b00000000000000000000";
        ram[101] = "0b00000000000000000000";
        ram[102] = "0b11111111111111111111";
        ram[103] = "0b11111111111111111111";
        for (unsigned i = 104; i < 110 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[110] = "0b11111111111111111111";
        ram[111] = "0b00000000000000111110";
        for (unsigned i = 112; i < 117 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[117] = "0b11111111111011000110";
        ram[118] = "0b00000000000000000000";
        ram[119] = "0b00000000000000000000";
        ram[120] = "0b00000000000000000000";
        ram[121] = "0b00000000000100110011";
        ram[122] = "0b00000000000000001100";
        ram[123] = "0b00000000000000000000";
        ram[124] = "0b00000000000000000000";
        ram[125] = "0b00000000000000000000";
        ram[126] = "0b00000000000000000000";
        ram[127] = "0b00000000000000001010";
        ram[128] = "0b00000000000000000000";
        ram[129] = "0b00000000000000000000";
        ram[130] = "0b00000000000000000000";
        ram[131] = "0b00000000000000000000";
        ram[132] = "0b00000000000000000001";
        ram[133] = "0b00000000000000000000";
        ram[134] = "0b00000000000000000000";
        ram[135] = "0b00000000000000000000";
        ram[136] = "0b00000000001011001101";
        for (unsigned i = 137; i < 142 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[142] = "0b00000000000000000001";
        ram[143] = "0b11111111111111111111";
        ram[144] = "0b00000000000000000000";
        ram[145] = "0b00000000000000000000";
        ram[146] = "0b00000000000000000110";
        ram[147] = "0b00000000000000000000";
        ram[148] = "0b00000000000000000000";
        ram[149] = "0b00000000000000000000";
        ram[150] = "0b00000000000000000010";
        ram[151] = "0b00000000000000000000";
        ram[152] = "0b00000000000000010001";
        ram[153] = "0b00000000000000000000";
        ram[154] = "0b00000000000000010110";
        for (unsigned i = 155; i < 160 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[160] = "0b00000000000001001100";
        ram[161] = "0b00000000000000001111";
        ram[162] = "0b00000000000000000000";
        ram[163] = "0b00000000000100000001";
        ram[164] = "0b00000000000000000100";
        ram[165] = "0b00000000000000000000";
        ram[166] = "0b00000000000000000000";
        ram[167] = "0b00000000000000000000";
        ram[168] = "0b11111111111111111111";
        ram[169] = "0b11111111111111111111";
        ram[170] = "0b00000001101001110100";
        ram[171] = "0b00000000000000000000";
        ram[172] = "0b11111111111111101111";
        ram[173] = "0b00000000010000010110";
        for (unsigned i = 174; i < 182 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[182] = "0b01010110010110010100";
        ram[183] = "0b00000000000000000000";
        ram[184] = "0b11111111111111111100";
        ram[185] = "0b00000000000000000000";
        ram[186] = "0b00000000000000000000";
        ram[187] = "0b11111111111111111111";
        ram[188] = "0b00000000000000000000";
        ram[189] = "0b00000000000000000000";
        ram[190] = "0b00000000000000000000";
        ram[191] = "0b00000000000100011000";
        ram[192] = "0b00000000000000000000";
        ram[193] = "0b00000000000000000000";
        ram[194] = "0b00000000000000000000";
        ram[195] = "0b00000000001100110110";
        for (unsigned i = 196; i < 204 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[204] = "0b11111111111111111111";
        ram[205] = "0b11111111111111111111";
        ram[206] = "0b00000000000000000000";
        ram[207] = "0b00000000000000000000";
        ram[208] = "0b11111111111111111111";
        ram[209] = "0b00000000000000000000";
        ram[210] = "0b00000000000000000000";
        ram[211] = "0b00000000000000000000";
        ram[212] = "0b11111111111111111111";
        ram[213] = "0b00000000000000000000";
        ram[214] = "0b00000000000000000000";
        ram[215] = "0b00000000000000000000";
        ram[216] = "0b00000000000000000000";
        ram[217] = "0b11111111111111111111";
        ram[218] = "0b00000000000010101001";
        ram[219] = "0b00000000000000000000";
        ram[220] = "0b11111111111111111111";
        ram[221] = "0b00000000000001111010";
        ram[222] = "0b00000000000000000000";
        ram[223] = "0b00000000000000001111";
        ram[224] = "0b11111111111111111111";
        ram[225] = "0b00000000000000000000";
        ram[226] = "0b00000000000001010110";
        ram[227] = "0b00000000000000000000";
        ram[228] = "0b11111111111111111111";
        ram[229] = "0b00000000000000000000";
        ram[230] = "0b00000000000000000000";
        ram[231] = "0b00000000000000000000";
        ram[232] = "0b00000000000000000000";
        ram[233] = "0b11111111111111111111";
        for (unsigned i = 234; i < 239 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[239] = "0b00000000000000001111";
        ram[240] = "0b00000000000000000000";
        ram[241] = "0b11000010110001010010";
        ram[242] = "0b00000000000000000000";
        ram[243] = "0b00000000000000000000";
        ram[244] = "0b11111111111110010000";
        for (unsigned i = 245; i < 250 ; i = i + 1) {
            ram[i] = "0b00000000000000000000";
        }
        ram[250] = "0b11111111111111111111";
        ram[251] = "0b00000000000000000000";
        ram[252] = "0b00000000000000000000";
        ram[253] = "0b00000000000000000000";
        ram[254] = "0b00000000000000110110";
        ram[255] = "0b11111111111111111111";


SC_METHOD(prc_write_0);
  sensitive<<clk.pos();
   }


void prc_write_0()
{
    if (ce0.read() == sc_dt::Log_1) 
    {
            if(address0.read().is_01() && address0.read().to_uint()<AddressRange)
              q0 = ram[address0.read().to_uint()];
            else
              q0 = sc_lv<DataWidth>();
    }
}


}; //endmodule


SC_MODULE(dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V) {


static const unsigned DataWidth = 20;
static const unsigned AddressRange = 256;
static const unsigned AddressWidth = 8;

sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in<sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V_ram* meminst;


SC_CTOR(dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V) {
meminst = new dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V_ram("dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V_ram");
meminst->address0(address0);
meminst->ce0(ce0);
meminst->q0(q0);

meminst->reset(reset);
meminst->clk(clk);
}
~dense_large_stream_me_ap_fixed_ap_fixed_config53_s_w53_V() {
    delete meminst;
}


};//endmodule
#endif
